#include <atomic>
#include <iostream>
#include <thread>

struct Object {
  void Foo() {
  }
};

void Race() {
  auto shared_ptr = std::make_shared<Object>();

  std::thread writer([&]() {
    auto new_ptr = std::make_shared<Object>();

    shared_ptr = new_ptr;
  });

  std::thread reader([&]() {
    // Try to increment ref count:
    // 1) read ptr to control block
    // 2) access ref count in control block (ctrl_block->ref_count++)
    auto ptr = shared_ptr;
    ptr->Foo();
  });

  writer.join();
  reader.join();
}

int main() {
  while (true) {
    Race();
  }
  return 0;
}
