#pragma once

#include <twist/stdlike/atomic.hpp>

#include <optional>

namespace lockfree {

// Treiber unbounded lock-free stack
// https://en.wikipedia.org/wiki/Treiber_stack

template <typename T>
class TreiberStack {
  struct Node {
    T value;
    Node* next{nullptr};
  };

 public:
  ~TreiberStack() {
    DeleteList(top_.load());
  }

  void Push(T value) {
    auto node = new Node{std::move(value)};
    node->next = top_.load();

    while (!top_.compare_exchange_weak(node->next, node)) {
      ;
    }
  }

  // heap-use-after-free:

  // ->A->B->C
  // T1: curr_top = A
  // T2: curr_top = A
  // T1: curr_top->next = B
  // T1: top_.compare_exchange(A, B)
  // T1: delete A
  // T2: A->next

  std::optional<T> TryPop() {
    while (true) {
      Node* curr_top = top_.load();
      if (curr_top == nullptr) {
        return std::nullopt;
      }
      if (top_.compare_exchange_weak(curr_top, curr_top->next)) {
        // Success!
        T popped_value = std::move(curr_top->value);
        // delete curr_top;
        return popped_value;
      }
    }
  }

 private:
  static void DeleteList(Node* head) {
    while (head != nullptr) {
      Node* to_delete = head;
      head = head->next;
      delete to_delete;
    }
  }

 private:
  twist::stdlike::atomic<Node*> top_{nullptr};
};

}  // namespace lockfree
